import logging
from flask import Flask, make_response, request
import requests
import json 

app = Flask(__name__)

# Configurar el logger para guardar los mensajes en un archivo
logging.basicConfig(filename='server.log', level=logging.INFO)
logger = logging.getLogger()

# Declaracion de puertos de servicios
port_producto = 5000
port_usuario = 5001
port_ventas = 5002
port_track = 5005

# URL de la API del backend
ip_back = 'http://lballgroups-643704480.us-east-1.elb.amazonaws.com'


@app.route('/Login', methods=['POST'])
def login():
    url = ip_back + ':' + str(port_usuario) + '/Login'
    print(request.data)
    response = requests.post(url,data=json.loads(request.data))
    return response.json()


@app.route('/Cliente/Registrar', methods=['POST'])
def cliente_registro():
    url = ip_back + ':' + str(port_usuario) + '/Cliente/Registrar'
    response = requests.post(url,data=json.loads(request.data))
    return response.json()


@app.route('/Proveedor/Registrar', methods=['POST'])
def proveedor_registro():
    url = ip_back + ':' + str(port_usuario) + '/Proveedor/Registrar'
    data=json.loads(request.data)
    print(data) 
    response = requests.post(url,data)
    return response.json()


@app.route('/Cliente/Comprar', methods=['POST'])
def cliente_compra():
    url = ip_back + ':' + str(port_ventas) + '/Cliente/Comprar'
    data = json.loads(request.data.decode('utf-8'))
    headers = {'Content-Type': 'application/json'}

    response = requests.post(url,json.dumps(data), headers=headers)
    return response.json()

@app.route('/Proveedor/CrearProducto', methods=['POST'])
def proveedor_producto():
    url = ip_back + ':' + str(port_producto) + '/Proveedor/CrearProducto'
    response = requests.post(url,data=json.loads(request.data))
    return response.json()


@app.route('/Inventario/GetProductos', methods=['GET'])
def inventario_productos():
    url = ip_back + ':' + str(port_producto) + '/Inventario/GetProductos'
    response = requests.get(url)
    return response.json()


@app.route('/Tarjeta/Crear', methods=['POST'])
def tarjeta_crear():
    url = ip_back + ':' + str(port_usuario) + '/Tarjeta/Crear'
    response = requests.post(url,data=json.loads(request.data))
    return response.json()


@app.route('/Tarjeta/Listado', methods=['POST'])
def tarjeta_listado():
    url = ip_back + ':' + str(port_usuario) + '/Tarjeta/Listado'
    response = requests.post(url,data=json.loads(request.data))
    return response.json()


# Falta añadir rutas para tracking

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=5000)
