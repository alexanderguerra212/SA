import { DataTypes, Model } from 'sequelize';
import { db } from '../database/db';


export interface TrackingAttributes extends Model {
    id_tracking: number;
    state: string;
    date: string;
    location: string;
    id_sale: number;
    id_customer: number;
  }


  const Tracking = db.define<TrackingAttributes>('Tracking', {
    id_tracking: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    state: {
      type: DataTypes.STRING,
    },
    location: {
      type: DataTypes.STRING,
    },
    date: {
      type: DataTypes.STRING,
    },
    id_sale: {
        type: DataTypes.NUMBER,
    },
    id_customer: {
        type: DataTypes.NUMBER,
    }
  });
  
  export default Tracking;

