"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const db_1 = require("../database/db");
const Tracking = db_1.db.define('Tracking', {
    id_tracking: {
        type: sequelize_1.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    state: {
        type: sequelize_1.DataTypes.STRING,
    },
    location: {
        type: sequelize_1.DataTypes.STRING,
    },
    date: {
        type: sequelize_1.DataTypes.STRING,
    },
    id_sale: {
        type: sequelize_1.DataTypes.NUMBER,
    },
    id_customer: {
        type: sequelize_1.DataTypes.NUMBER,
    }
});
exports.default = Tracking;
